from flask import Flask, render_template, request, g, session

import gspread

import requests

import pandas as pd
import json

from dateutil.parser import parse, ParserError
from datetime import date

app = Flask(__name__)

google_service_account = gspread.service_account(filename='service_account_key.json')
worksheet = google_service_account.open('Currency exchange rate').get_worksheet(0)

@app.route('/', methods=('GET', 'POST'))
def index():
    response_table = ''

    if request.method == 'POST':
        date_from = request.form['date_from']
        date_to = request.form['date_to']
        
        try:
            date_from = parse(date_from).date()
        except ParserError:
            date_from = date.today()
        
        print('date_from:', date_from)
        
        try:
            date_to = parse(date_to).date()
        except ParserError:
            date_to = date.today()
        
        print('date_to:', date_to)
        
        print('getting currency data')
        r = requests.get('https://bank.gov.ua/NBU_Exchange/exchange_site',
                         params=dict(start=date_from.strftime("%Y%m%d"),
                                     end=date_to.strftime("%Y%m%d"),
                                     valcode='usd',
                                     json=True)
                        )

        print('displaying table in page')
        df_table = pd.DataFrame(json.loads(r.text))
        display_table = df_table.to_html(classes='data', header='true', index=False)
        
        print('update data in google sheet')
        worksheet.clear()
        worksheet.update([df_table.columns.values.tolist()] + df_table.values.tolist())

    return render_template('index.html', display_table=display_table)
